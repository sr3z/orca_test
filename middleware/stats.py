import logging
import time
from django.utils.deprecation import MiddlewareMixin
from orca.models import RequestStat


logger = logging.getLogger(__name__)

class StatsMiddleware(MiddlewareMixin):
    def process_request(self, request):
        "Request start time"
        request.start_time = time.time()
        logger.info("STAT REQUEST: {}".format(request.start_time))

    def process_response(self, request, response):
        "Request end time"
        spent = time.time() - request.start_time
        logger.info("STAT RESPONSE: {}".format(spent))

        try:
            stat = RequestStat.objects.create(spent=spent, url=request.path_info)
            logger.info("STAT: {}".format(stat))
        except Exception as e:
            logger.error("STAT: {}".format(e))

        return response
