from rest_framework import serializers
from orca.models import VM, Tag, FWRule
from django.core.exceptions import ObjectDoesNotExist


class SlugRelatedField(serializers.SlugRelatedField):
    """
    Field to be created via slug rather than id
    """
    def to_internal_value(self, data):
        try:
            return self.get_queryset().get_or_create(**{self.slug_field: data})[0]
        except ObjectDoesNotExist:
            self.fail('does_not_exist', name=self.slug_field, value=data)
        except (TypeError, ValueError):
            self.fail('invalid')


class TagSerializer(serializers.ModelSerializer):
    """
    Tag serializer
    """

    class Meta:
        model = Tag
        fields = (
            "name",
        )
        read_only_fields = ("id",)


class VMSerializer(serializers.ModelSerializer):
    """
    VM serializer
    """

    tags = SlugRelatedField(
        many=True,
        slug_field='name',
        queryset=Tag.objects.all()
    )

    class Meta:
        model = VM
        fields = (
            "name",
            "vm_id",
            "tags",
        )
        read_only_fields = ("id",)


class FWRuleSerializer(serializers.ModelSerializer):
    """
    Firewall Rule serializer
    """

    source_tag = SlugRelatedField(
        many=False,
        slug_field='name',
        queryset=Tag.objects.all()
    )

    dest_tag = SlugRelatedField(
        many=False,
        slug_field='name',
        queryset=Tag.objects.all()
    )

    class Meta:
        model = FWRule
        fields = (
            "fw_id",
            "source_tag",
            "dest_tag",
        )
        read_only_fields = ("id",)
