import json
from orca.models import VM, FWRule, Tag
import pprint
import logging
from orca.serializers import VMSerializer, FWRuleSerializer, TagSerializer


logger = logging.getLogger(__name__)

def populate_data(filename):
    """
    populate test data form the file provided
    """
    data_list = None
    with open(filename) as f:
        data_list = json.load(f)

    if not data_list:
        logger.error("loading data failed\n")
        return

    for k, v in data_list.items():
        if k == "vms":
            logger.debug("VM: {}".format(v))
            for i in v:
                serializer = VMSerializer(data=i)
                if not serializer.is_valid(): 
                    logger.warning("BAD DATA FOR VM: {}".format(serializer.errors))
                    continue
                serializer.save()
        elif k == "fw_rules":
            logger.debug("FWRule: {}".format(v))
            for i in v:
                serializer = FWRuleSerializer(data=i)
                if not serializer.is_valid(): 
                    logger.warning("BAD DATA FOR FWRule: {}".format(serializer.errors))
                    continue
                serializer.save()

    objs = VM.objects.all()
    for o in objs:
        logger.warning("{} [{}]".format(o, o.tags.all()))

    objs = FWRule.objects.all()
    for o in objs:
        logger.warning(pprint.pformat(o))

    objs = Tag.objects.all()
    for o in objs:
        logger.warning("{}".format(o))
