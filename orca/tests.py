from django.test import TestCase
from django.urls import reverse
from orca.models import VM, Tag, FWRule
import logging


logger = logging.getLogger(__name__)

class APITestCase(TestCase):
    """
    Application API test
    """

    def setUp(self):
        source_tag = Tag.objects.create(name="windows")
        dest_tag = Tag.objects.create(name="ssh")
        vmc = VM.objects.create(vm_id="carnivore")
        vmv = VM.objects.create(vm_id="victim")
        vmc.tags.add(source_tag)
        vmv.tags.add(dest_tag)
        fw = FWRule.objects.create(source_tag=source_tag, dest_tag=dest_tag)

    def test_attack(self):
        response = self.client.post(reverse("attack"))
        self.assertEqual(response.status_code, 405)

        response = self.client.post(reverse("attack") + "?vm_id=")
        self.assertEqual(response.status_code, 405)

        response = self.client.post(reverse("attack") + "?vm_id=victim")
        self.assertEqual(response.status_code, 405)

        response = self.client.get(reverse("attack"))
        self.assertEqual(response.status_code, 400)

        response = self.client.get(reverse("attack") + "?vm_id=victim")
        logger.warning("ATTACK TEST: CODE: {} VALUE: {}".format(response.status_code, response.content))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content.decode(), '["carnivore"]')

    def test_stats(self):
        response = self.client.get(reverse("stats"))
        logger.warning("STATS TEST: CODE: {} VALUE: {}".format(response.status_code, response.content))
        self.assertEqual(response.status_code, 200)
