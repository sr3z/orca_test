from django.db import models
from django.utils.translation import gettext_lazy as _
from decimal import Decimal


class VM(models.Model):
    name = models.CharField(_("name"), max_length=32)
    vm_id = models.CharField(_("vm_id"), max_length=32, unique=True)

    class Meta:
        verbose_name = _("VM")
        verbose_name_plural = _("VM")

    def __str__(self):
        return "#{}: {}: {}".format(self.id, self.vm_id, self.name)


class Tag(models.Model):
    name = models.CharField(_("name"), max_length=32, unique=True)
    vms = models.ForeignKey(VM, related_name="tags", on_delete=models.SET_NULL, blank=True, null=True)

    class Meta:
        verbose_name = _("Tag")
        verbose_name_plural = _("Tags")

    def __str__(self):
        return "#{}: {}".format(self.id, self.name)


class FWRule(models.Model):
    fw_id = models.CharField(_("vm_id"), max_length=32, unique=True)
    source_tag = models.ForeignKey(Tag, related_name="source_tags", on_delete=models.SET_NULL, blank=True, null=True)
    dest_tag = models.ForeignKey(Tag, related_name="dest_tags", on_delete=models.SET_NULL, blank=True, null=True)

    class Meta:
        verbose_name = _("FWRule")
        verbose_name_plural = _("FWRule")

    def __str__(self):
        return "{}: {}=>{}".format(self.fw_id, self.source_tag, self.dest_tag)


class RequestStat(models.Model):
    spent = models.DecimalField(max_digits=20, decimal_places=6, default=Decimal(0.00))
    url = models.CharField(_("URL"), max_length=1024, null=True, blank=True)

    def __str__(self):
        return "{}: {}".format(self.spent, self.url)
