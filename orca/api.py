from orca.models import RequestStat, VM, FWRule, Tag
from rest_framework.decorators import api_view
from django.http import JsonResponse, HttpResponseServerError, HttpResponseBadRequest, HttpResponse
from django.db.models import Count, Avg, Sum
import logging


logger = logging.getLogger(__name__)

@api_view(http_method_names=["GET"])
def attack(request):
    """ attack request """

    vm_id = request.GET.get("vm_id")
    if not vm_id:
        return HttpResponseBadRequest("ID is not specified")
    obj = VM.objects.filter(vm_id=vm_id).first()
    if not obj:
        return HttpResponseBadRequest("Incorrect ID")
    logger.warning("TESTED VM: {}".format(obj))

    # dtags = list(obj.tags.all().values_list("id", flat=True))
    # logger.warning("VM TAGS: {}".format(dtags))
    # stags = list(FWRule.objects.filter(dest_tag__in=dtags).values_list("source_tag", flat=True))
    # logger.warning("SRC TAGS: {}".format(stags))
    # a = list(VM.objects.filter(tags__in=stags).exclude(vm_id=vm_id).values_list("vm_id", flat=True))
    # logger.warning("ATTACK VMS: {}".format(a))

    aa = VM.objects.raw("SELECT a.* FROM orca_vm a WHERE NOT vm_id = %s AND id IN ( \
                            SELECT vms_id FROM orca_tag WHERE id IN ( \
                                SELECT source_tag_id FROM orca_fwrule f, orca_tag t, orca_vm v  \
                                WHERE f.dest_tag_id = t.id AND t.vms_id = v.id AND vm_id = %s))", [vm_id, vm_id])
    a = []
    for i in aa:
        a.append(i.vm_id)
    logger.warning("ATTACK VMS: {}".format(a))
    return JsonResponse(a, safe=False)

@api_view(http_method_names=["GET"])
def stats(request):
    """ stats request """

    values = VM.objects.all().aggregate(count=Count("id"))
    logger.warning("VALUES: {}".format(values))
    stats = RequestStat.objects.all().aggregate(avg=Avg("spent"), count=Count("id"))
    logger.warning("STATS: {}".format(stats))
    return JsonResponse({"vm_count":values.get("count"),"request_count":stats.get("count"),"average_request_time": float(stats.get("avg") or 0)})
